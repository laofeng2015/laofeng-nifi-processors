import junit.framework.TestCase;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.laofeng.nifi.processors.OcrTesseractProcessor;


public class TestTessjTest extends TestCase {

    public void testProcessor(){
        TestRunner runner = TestRunners.newTestRunner(new OcrTesseractProcessor());
        runner.setProperty("tess lang path","/usr/local/Cellar/tesseract-lang/4.0.0/share/tessdata/");
        runner.setProperty("tess lib path","/usr/local/Cellar/tesseract/4.1.1/share/tessdata");
        runner.run();
    }

}