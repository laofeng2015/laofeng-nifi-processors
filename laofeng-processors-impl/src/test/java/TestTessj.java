import junit.framework.TestCase;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;


public class TestTessj extends TestCase {

    public void testOcr()throws IOException, TesseractException{
        System.setProperty("jna.library.path","/usr/local/Cellar/tesseract/4.1.1/lib");
        ITesseract instance = new Tesseract();
        instance.setDatapath("/usr/local/Cellar/tesseract-lang/4.0.0/share/tessdata/");
        //instance.setDatapath("/usr/local/Cellar/tesseract/4.1.1/share/tessdata");
        instance.setLanguage("chi_sim");
        String words = instance.doOCR(ImageIO.read(new File("/Users/laofeng/Downloads/apps/data_out/best-practices-for-the-data-lake-2-1024.jpg")));
        System.out.println(words);
    }


}
