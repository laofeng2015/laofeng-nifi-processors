import junit.framework.TestCase;
import org.json.JSONArray;
import org.json.JSONObject;

public class TestJsonObject extends TestCase {

    public void testJson(){
        String json_str = "     {\n" +
                "            \"log_id\": 8468047568305872599,\n" +
                "                \"words_result\": [\n" +
                "            {\"words\": \" Two factor authentication is not enabled yet.\"},\n" +
                "            {\"words\": \" Two-factor authentication adds an additional layer of security to your account by requiring more than\"},\n" +
                "            {\"words\": \" just password to log in.\"}\n" +
                "  ],\n" +
                "            \"words_result_num\": 3\n" +
                "        }";

        JSONObject jo = new JSONObject(json_str);

        System.out.println(export_words(jo));

    }

    private String export_words(JSONObject jo) {

                JSONArray words_result = jo.getJSONArray("words_result");
        //JSONArray words = words_result.getJSONArray("words");
        int size = words_result.length();
        StringBuilder buff = new StringBuilder();
        for (int i = 0; i < size; i++){
            JSONObject word = words_result.getJSONObject(i);
            buff.append(word.getString("words")).append("\n");
        }
        buff.deleteCharAt(buff.length() - 1);

        return buff.toString();
    }
}
